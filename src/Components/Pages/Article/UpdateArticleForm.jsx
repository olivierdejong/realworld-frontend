import React, {useEffect, useState} from "react";
import {useHistory, useParams} from "react-router-dom";
import API from "../../../Utils/API";
import Form from "../../Parts/Form/Form";
import Input from "../../Parts/Form/Input/Input";
import Textarea from "../../Parts/Form/Textarea/Textarea";
import Submit from "../../Parts/Form/Submit/Submit";
import Error from "../../Parts/Form/Error/Error";

function objectsToString(arrayOfObjects) {
  if (!arrayOfObjects) {
    return null
  }

  return arrayOfObjects.map(tag => tag.name).join(', ');
}

const initialData = {
  article: {},
  error: ''
};

function UpdateArticleForm() {
  const [data, setData] = useState(initialData);
  const history = useHistory();
  const {slug} = useParams();

  useEffect(() => {
    async function fetchData() {
      try {
        const response = await API.get(`articles/${slug}`);
        setData({
          article: {
            ...response.data.article,
            tagList: objectsToString(response.data.article.tagList)
          },
          error: '',
        });
      } catch (error) {
        setData(previousData => {
            return {
              ...previousData,
              error: error.response.data.errors
            };
          }
        )
      }
    }

    fetchData();
  }, [slug]);

  function handleChange(e) {
    const { name, value } = e.target;

    setData({
      ...data,
      article: {
        ...data.article,
        [name]: value,
      }
    });
  }

  const removeFalsy = (obj) => {
    const newObj = {};
    Object.keys(obj).forEach((prop) => {
      if (obj[prop]) {
        newObj[prop] = obj[prop];
      }
    });
    return newObj;
  };

  function handleUpdate() {
    let { article } = data;
    if (article.tagList) {
      article = {
        ...article,
        tagList: article.tagList.split(', ')
      };
    }

    const filteredArticle = removeFalsy(article);

    try {
      API.put(`articles/${data.article.slug}`, filteredArticle);
      history.push('/myprofile');

    } catch (error) {
      setData({
        ...data,
        error: error.response.data.errors
      });
    }
  }

  return (
    <Form h2Title='update article' halfwidth>
      {data.article.title &&
      <>
        <Input name='title' value={data.article.title} onValueChange={handleChange}/>
        {data.error.title && <Error error={data.error.title}/>}
        <Input name='description' value={data.article.description} onValueChange={handleChange}/>
        {data.error.description && <Error error={data.error.description}/>}
        <Textarea name='body' value={data.article.body} onValueChange={handleChange}/>
        {data.error.body && <Error error={data.error.body}/>}
        <Input splitComma name='tagList' value={data.article.tagList} onValueChange={handleChange}/>
        {data.error.tagList && <Error error={data.error.tagList}/>}
        <Submit text='update' onClick={handleUpdate}/>
      </>}
    </Form>
  );
}

export default UpdateArticleForm;