import React from 'react';
import Layout from "../../Layout/Layout";
import UpdateArticleForm from "./UpdateArticleForm";

const banner = {
  title: 'Update Article',
  subText: 'As if it was not good enough already!',
};

function UpdateArticle() {
  return (
    <Layout banner={banner}>
      <UpdateArticleForm />
    </Layout>
  );
}

export default UpdateArticle;