import React, { useEffect, useState} from 'react';
import { useParams } from 'react-router-dom';
import Layout from '../../Layout/Layout';
import { Flex } from '../../Layout/Wrapper';
import ArticleInfo from './Parts/ArticleInfo';
import ArticleContent from './Parts/ArticleContent';
import AuthorCard from './Parts/AuthorCard';
import CommentSection from './Comments/CommentSection';
import API from '../../../Utils/API';

const banner = {
  title: 'Article',
  subText: 'This is where you can read a single article!'
};

const initialData = {
  article: {},
  author: {},
  errorMessage: '',
};

function Article() {
  const [data, setData] = useState(initialData);
  const { slug } = useParams();

  useEffect(() => {
    async function fetchData() {
      try {
        const response = await API.get(`articles/${slug}`);
        setData({
          article: response.data.article,
          author: response.data.article.author,
          errorMessage: '',
        });
      }

      catch (error) {
        setData({
          article: {},
          author: {},
          errorMessage: error.response
        });
      }
    }

    fetchData();
  }, [slug]);

  return (
    <Layout banner={banner}>
      <Flex wide box>
        {data.errorMessage && <span>{data.errorMessage}</span>}
        {data.article.id &&
        <>
          <ArticleInfo article={data.article} />
          <ArticleContent article={data.article} />
          <AuthorCard author={data.author} />
          <CommentSection article={data.article} />
        </>}
      </Flex>
    </Layout>
  );
}

export default Article;
