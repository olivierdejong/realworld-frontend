import React, {useState} from "react";
import {useHistory} from "react-router-dom";
import API from "../../../Utils/API";
import Form from "../../Parts/Form/Form";
import Input from "../../Parts/Form/Input/Input";
import Textarea from "../../Parts/Form/Textarea/Textarea";
import Submit from "../../Parts/Form/Submit/Submit";
import Error from "../../Parts/Form/Error/Error";

const initialData = {
  article: {
    title: '',
    description: '',
    body: '',
    tagList: '',
  },
  error: '',
};

function CreateArticleForm() {
  const [data, setData] = useState(initialData);
  const history = useHistory();

  function handleChange(e) {
    const { name, value } = e.target;

    setData({
      ...data,
      article: {
        ...data.article,
        [name]: value,
      }
    });
  }

  async function handleCreate() {
    let { article } = data;
    if (article.tagList) {
      article = {
        ...article,
        tagList: article.tagList.split(', ')
      };
    }

    try {
      await API.post(`articles`, article);
      history.push(`/`);

    } catch (error) {
      setData({
        ...data,
        error: error.response.data.errors
      });
    }
  }

  return (
    <Form h2Title='create article'>
      <Input name='title' onValueChange={handleChange}/>
      {data.error.title && <Error error={data.error.title}/>}
      <Input name='description' onValueChange={handleChange}/>
      {data.error.description && <Error error={data.error.description}/>}
      <Textarea name='body' onValueChange={handleChange}/>
      {data.error.body && <Error error={data.error.body}/>}
      <Input type='tagList' name='tagList' splitComma value={data.article.tagList} onValueChange={handleChange}/>
      {data.error.tagList && <Error error={data.error.tagList}/>}
      <Submit text='create' onClick={handleCreate}/>
    </Form>
  );
}

export default CreateArticleForm;