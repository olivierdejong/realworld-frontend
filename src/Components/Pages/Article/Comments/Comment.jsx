import React, {useContext} from 'react';
import moment from "moment";
import PropTypes from "prop-types";
import {Link} from "react-router-dom";
import {Block, Flex} from '../../../Layout/Wrapper';
import profileIcon from "../../../../Utils/Images/profileIcon.png";
import {AuthContext} from "../../../../App";

function Comment({ comment, clickDelete }) {
  const {isAuthenticated, user} = useContext(AuthContext).state;

  const { id, createdAt, body, author } = comment;
  const {username, image} = author;
  const authorized = isAuthenticated && user.username === comment.author.username;

  return (
    <Block border='1px lightgrey solid'>
      <p style={{padding:'1.5rem'}}>{body}</p>
      <Flex row align='center' style={{backgroundColor:'#F5F5F5'}}>
        <Flex row align='center' justify='spaceBetween'>
          <Link style={{lineHeight:'0', padding: '0.6rem'}} to={`/profiles/${username}`}>
            <img style={{borderRadius:'100%', width: '2.2rem', height: '2.2rem', marginRight: '0.3rem'}} src={image || profileIcon} alt="Logo"/>
          </Link>
          <Link to={`/profiles/${username}`}><h4>{username}</h4></Link>
          {createdAt && <h6 style={{padding:'0.5rem', color:'grey'}} >{moment(createdAt).format('MMM Do, YYYY')}</h6>}
        </Flex>
        {authorized && <button type='button' style={{backgroundColor:'red', height:'1rem', width:'1rem', margin:'1rem'}} onClick={(() =>clickDelete(id))}>x</button>}
      </Flex>
    </Block>
  );
}

Comment.propTypes = {
  comment: PropTypes.shape({
    id: PropTypes.number,
    createdAt: PropTypes.string,
    body: PropTypes.string,
    author: PropTypes.shape({
      username: PropTypes.string,
      image: PropTypes.string,
    }),
  }),
  clickDelete: PropTypes.func,
};

export default Comment;