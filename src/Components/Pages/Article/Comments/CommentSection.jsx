import React, {useState, useEffect, useContext} from 'react';
import PropTypes from 'prop-types';
import {Grid} from '../../../Layout/Wrapper';
import {AuthContext} from '../../../../App';
import Comment from './Comment';
import API from '../../../../Utils/API';

const initialData = {
  comments: [],
  errorMessage: '',
};

const inputStyle = {
  width:'100%',
  height:'4rem',
  margin:'1rem 0 0',
  padding: '1rem 2rem',
  fontSize: '1rem',
};

function CommentSection({ article }) {
  const {isAuthenticated} = useContext(AuthContext).state;
  const [data, setData] = useState(initialData);
  const [input, setInput] = useState('');

  useEffect(() => {
    async function fetchData() {
      try {
        const response = await API.get(`articles/${article.slug}/comments`);
        setData({
          comments: response.data.comments,
          errorMessage: '',
        });
      } catch (error) {
        setData({
          comments: [],
          errorMessage: error.data,
        });
      }
    }

    fetchData();
  }, [article]);

  function handleChange(e) {
    setInput(e.target.value);
  }

  async function handleSubmit(e) {
    e.preventDefault();
    try {
      const response = await API.post(`articles/${article.slug}/comments`, {body: input});
      setInput('');
      setData(previousData => {
        return {
          ...previousData,
          comments: [
            response.data.comment,
            ...previousData.comments,
          ]
        }
      });

      // TODO: fix error
    } catch (error) {
      setData({
        comments: [],
        errorMessage: error.data,
      });
    }
  }

  function deleteComment(id) {
    try {
      API.delete(`articles/${article.slug}/comments/${id}`);
      setData(previousData => {
        const newData = previousData.comments.filter(comment => comment.id !== id);
        return {
          comments: newData,
          errorMessage: '',
        }
      });

    } catch (error) {
      setData({
        comments: [],
        errorMessage: error.data,
      });
    }
  }

  return (
    <Grid margin='2rem auto' wide rowGap='1rem'>
      {isAuthenticated && <form onSubmit={handleSubmit} autoComplete='off'>
        <input style={inputStyle} name='comment' type='text' placeholder='New comment..' value={input} onChange={handleChange}/>
      </form>}
      {data.comments &&
      data.comments.map((comment) => <Comment key={comment.id} comment={comment} clickDelete={deleteComment}/>)}
    </Grid>
  );
}

CommentSection.propTypes = {
  article: PropTypes.shape({
    slug: PropTypes.string,
  }),
};

export default CommentSection;
