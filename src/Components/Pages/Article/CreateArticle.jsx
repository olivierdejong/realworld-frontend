import React from 'react';
import Layout from "../../Layout/Layout";
import CreateArticleForm from './CreateArticleForm';

function CreateArticle() {
  return (
    <Layout>
      <CreateArticleForm />
    </Layout>
  );
}

export default CreateArticle;