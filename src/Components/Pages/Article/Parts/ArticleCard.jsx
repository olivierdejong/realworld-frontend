import React from 'react';
import PropTypes from 'prop-types';
import { Block } from '../../../Layout/Wrapper';
import UsernameBanner from './UsernameBanner';
import ArticleTitleBanner from './ArticleTitleBanner';
import TagsContainer from "../Tags/TagsContainer";

function ArticleCard({ article }) {
  const { author, description, tagList } = article;

  return (
    <Block borderBottom='1px lightgrey solid' padding='1rem 0'>
      <UsernameBanner profile={author} article={article} />
      <ArticleTitleBanner article={article}/>
      <p>{description}</p>
      {article.tagList &&
      <TagsContainer tags={tagList} padding='1rem 0 0'/>}
    </Block>
  );
}

ArticleCard.propTypes = {
  article: PropTypes.shape({
    slug: PropTypes.string,
    title: PropTypes.string,
    createdAt: PropTypes.string,
    description: PropTypes.string,
    tagList: PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.number,
      name: PropTypes.string,
    })),
    favorited: PropTypes.bool,
    favoritesCount: PropTypes.number,
    author: PropTypes.shape({
      username: PropTypes.string,
      bio: PropTypes.string,
      image: PropTypes.string,
      following: PropTypes.bool
    }),
  })
};

export default ArticleCard;
