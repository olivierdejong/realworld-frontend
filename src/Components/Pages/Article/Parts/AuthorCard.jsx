import React from 'react';
import PropTypes from 'prop-types';
import UsernameBanner from './UsernameBanner';
import { Block } from '../../../Layout/Wrapper';

function AuthorCard({ author }) {
  return (
    <Block borderTop='1px lightgrey solid' padding='1rem 0'>
      <UsernameBanner profile={author} />
      <p>{author.bio}</p>
    </Block>
  );
}

AuthorCard.propTypes = {
  author: PropTypes.shape({
    username: PropTypes.string,
    bio: PropTypes.string,
    image: PropTypes.string,
  }),
};

export default AuthorCard;
