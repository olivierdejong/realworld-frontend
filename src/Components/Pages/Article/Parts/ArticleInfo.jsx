import React, {useContext} from 'react';
import moment from 'moment';
import PropTypes from 'prop-types';
import Favorite from '../../../Parts/Button/Favorite';
import {Flex} from '../../../Layout/Wrapper'
import {AuthContext} from '../../../../App';
import Edit from "../../../Parts/Button/Edit";

function ArticleInfo({article}) {
  const {isAuthenticated, user} = useContext(AuthContext).state;
  const {title, createdAt, description, author} = article;

  const button = (isAuthenticated && author.username === user.username)
    ? <Edit text='Edit Article'/>
    : <Favorite article={article}/>;

  return (
    <Flex row wide>
      <Flex box align='flex-start'>
        <h1>{title}</h1>
        <p>{description}</p>
      </Flex>
      <Flex box>
        <h5>{moment(createdAt).format('MMM Do, YYYY')}</h5>
        {isAuthenticated
        && button}
      </Flex>
    </Flex>
  );
}

ArticleInfo.propTypes = {
  article: PropTypes.shape({
    title: PropTypes.string,
    createdAt: PropTypes.string,
    description: PropTypes.string,
    author: PropTypes.shape({
      username: PropTypes.string,
    }),
  })
};

export default ArticleInfo;
