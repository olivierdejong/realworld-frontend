import React from 'react';
import {Link} from 'react-router-dom';
import PropTypes from "prop-types";
import {Flex} from '../../../Layout/Wrapper';

function ArticleTitleBanner({article}) {
  return (
    <Flex row align='center'>
      <Link to={`/articles/${article.slug}`}><h2>{article.title}</h2></Link>
    </Flex>
  )
}

ArticleTitleBanner.propTypes = {
  article: PropTypes.shape({
    title: PropTypes.string,
    slug: PropTypes.string,
    createdAt: PropTypes.string,
    description: PropTypes.string,
  }),
};

export default ArticleTitleBanner;