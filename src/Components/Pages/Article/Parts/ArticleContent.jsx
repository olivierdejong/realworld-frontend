import React from 'react';
import PropTypes from 'prop-types';
import { Grid } from '../../../Layout/Wrapper';
import { StyledTextContainer } from "../../../Layout/Container";
import TagsContainer from "../Tags/TagsContainer";

function ArticleContent({ article }) {
  const { body, tagList } = article;
  return (
    <Grid margin='2rem auto' rowGap='0' wide>
      <StyledTextContainer>
        {body.split('\n')
          .filter(Boolean)
          .map( (section) => {
            return (
              <p key={section.slice(0,10) + section.slice(-10, -1)}>
                {section}
              </p>
            )
          })
        }
      </StyledTextContainer>
      <TagsContainer tags={tagList} justify='flex-end' />
    </Grid>
  );
}

ArticleContent.propTypes = {
  article: PropTypes.shape({
    body: PropTypes.string,
    tagList: PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.number,
      name: PropTypes.string,
    })),
  })
};

export default ArticleContent;
