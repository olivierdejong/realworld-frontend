import React, {useContext} from 'react';
import moment from 'moment';
import {Link} from 'react-router-dom';
import PropTypes from "prop-types";
import {Flex} from '../../../Layout/Wrapper';
import profileIcon from '../../../../Utils/Images/profileIcon.png';
import Favorite from "../../../Parts/Button/Favorite";
import {AuthContext} from "../../../../App";

function UsernameBanner({article, profile}) {
  const loggedIn = useContext(AuthContext).state.isAuthenticated;
  return (
    <Flex row align='center' padding='0 0 1rem'>
      <Flex row align='center'>
        <Link to={`/profiles/${profile.username}`}>
          <img style={{borderRadius:'100%', width: '2.2rem', height: '2.2rem', marginRight: '0.3rem'}} src={profile.image || profileIcon} alt="Logo"/>
        </Link>
        <Flex box align='flex-start'>
          <Link to={`/profiles/${profile.username}`}><h4>{profile.username}</h4></Link>
          {article && <h6>{moment(article.createdAt).format('MMM Do, YYYY')}</h6>}
        </Flex>
      </Flex>
      {(loggedIn && article) && <Favorite article={article}/>}
    </Flex>
  )
}

UsernameBanner.propTypes = {
  article: PropTypes.shape({
    title: PropTypes.string,
    createdAt: PropTypes.string,
    description: PropTypes.string,
  }),
  profile: PropTypes.shape({
    username: PropTypes.string,
    bio: PropTypes.string,
    image: PropTypes.string,
    following: PropTypes.bool
  }),
  createdAt: PropTypes.string,
};

export default UsernameBanner;