import React from 'react';
import PropTypes from 'prop-types';
import { StyledTag } from '../../../Parts/Button/ButtonStyle';

// TODO: DRY tags code
function Tag({ tag }) {
  return (
    <StyledTag
      discreet
      type='button'
      key={tag.id}>
      {tag.name}
    </StyledTag>
  );
}

Tag.propTypes = {
  tag: PropTypes.shape({
    id: PropTypes.number,
    name: PropTypes.string,
  }),
};

export default Tag;
