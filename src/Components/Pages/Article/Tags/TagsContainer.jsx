import React from 'react';
import PropTypes from "prop-types";
import Tag from "./Tag";
import {StyledTagsContainer} from '../../../Layout/Container';

const TagsContainer = ({tags, align, justify, padding}) => {
  return (
    <StyledTagsContainer align={align} justify={justify} padding={padding}>
      {tags.map((tag) => <Tag key={tag.id} tag={tag} discreet/>)}
    </StyledTagsContainer>
  );
};

TagsContainer.propTypes = {
  tags: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      name: PropTypes.string,
    })
  ),
  align: PropTypes.string,
  justify: PropTypes.string,
  padding: PropTypes.string,
};

export default TagsContainer;