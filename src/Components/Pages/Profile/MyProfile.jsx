import React, {useContext} from 'react';
import Layout from '../../Layout/Layout';
import { Grid } from '../../Layout/Wrapper';
import ProfileContent from './Parts/ProfileContent';
import FeedWrapper from '../../Parts/Feed/FeedWrapper';
import {AuthContext} from '../../../App';

const banner = {
  title: 'My Profile',
  subText: 'Man, you have such a nice profile!'
};

function MyProfile() {
  const { user } = useContext(AuthContext).state;
  return (
    <Layout banner={banner}>
      <Grid rowGap='2rem' wide>
        <ProfileContent profile={user}/>
        <FeedWrapper profile={user}/>
      </Grid>
    </Layout>
  );
}

export default MyProfile;
