import React, {useContext} from 'react';
import PropTypes from 'prop-types';
import Follow from '../../../Parts/Button/Follow';
import Edit from '../../../Parts/Button/Edit';
import {Flex} from '../../../Layout/Wrapper'
import {AuthContext} from '../../../../App';

const imageStyle = {
  height:'10rem',
  maxWidth:'100vw',
  borderRadius:'10%'
};

function ProfileContent({profile}) {
  const {isAuthenticated, user} = useContext(AuthContext).state;
  const {username, bio, image} = profile;

  const button = (isAuthenticated && username === user.username)
    ? <Edit text='Edit Profile'/>
    : <Follow profile={profile}/>;

  return (
    <Flex box>
      {image && <img style={imageStyle} src={image} alt=''/>}
      <h2>{username}</h2>
      <p>{bio}</p>
      {isAuthenticated && button}
    </Flex>
  );
}

ProfileContent.propTypes = {
  profile: PropTypes.shape({
    username: PropTypes.string,
    bio: PropTypes.string,
    image: PropTypes.string,
    following: PropTypes.bool
  }),
};

export default ProfileContent;
