import React, { useContext, useEffect, useState } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import Layout from '../../Layout/Layout';
import { Grid } from '../../Layout/Wrapper';
import ProfileContent from './Parts/ProfileContent';
import FeedWrapper from '../../Parts/Feed/FeedWrapper';
import { AuthContext } from '../../../App';
import API from '../../../Utils/API';

const banner = {
  title: 'Profile',
  subText: 'Checkout this profile!'
};

function Profile() {
  const history = useHistory();
  const {username} = useParams();
  const {isAuthenticated, user} = useContext(AuthContext).state;
  const [myProfile, isMyProfile] = useState(false);
  const [data, setData] = useState({
    profile: {},
    errorMessage: ''
  });

  useEffect(() => {
    if (myProfile) {
      history.push('/myProfile')
    }
  }, [history, myProfile]);

  useEffect(() => {
    const fetchDataOrRedirect = (isAuthenticated && username === user.username)
      ? () => {isMyProfile(true)}
      : async () => {
        try {
          const response = await API.get(`profiles/${username}`);
          setData({
            profile: response.data.profile,
            errorMessage: ''
          });
        } catch (error) {
          setData({
            profile: '',
            errorMessage: error.response.data.error
          });
        }
      };

    fetchDataOrRedirect();
  }, [isAuthenticated, user, myProfile, username]);

  return (
    <Layout banner={banner}>
      <Grid rowGap='2rem' wide>
        {data.profile.username &&
        <>
          <ProfileContent profile={data.profile}/>
          <FeedWrapper profile={data.profile}/>
        </>}
      </Grid>
    </Layout>
  );
}

export default Profile;
