import React from 'react';
import Layout from '../../Layout/Layout';
import UpdateProfileForm from './UpdateProfileForm';

const banner = {
  title: 'Update Profile',
  subText: 'As if it was not good enough already!',
};

function UpdateProfile() {
  return (
    <Layout banner={banner}>
      <UpdateProfileForm />
    </Layout>
  )
}

export default UpdateProfile;