import React, {useContext, useState} from "react";
import {useHistory} from "react-router-dom";
import {AuthContext} from "../../../App";
import API from "../../../Utils/API";
import Form from "../../Parts/Form/Form";
import Input from "../../Parts/Form/Input/Input";
import Textarea from "../../Parts/Form/Textarea/Textarea";
import Submit from "../../Parts/Form/Submit/Submit";
import Error from "../../Parts/Form/Error/Error";

function UpdateProfileForm() {
  const {dispatch} = useContext(AuthContext);
  const {user} = useContext(AuthContext).state;
  const [data, setData] = useState({
    user,
    error: ''
  });
  const history = useHistory();

  function handleChange(e) {
    setData({
      ...data,
      user: {
        ...data.user,
        [e.target.name]: e.target.value,
      }
    });
  }

  const removeFalsy = (obj) => {
    const newObj = {};
    Object.keys(obj).forEach((prop) => {
      if (obj[prop]) {
        newObj[prop] = obj[prop];
      }
    });
    return newObj;
  };

  async function handleUpdate() {
    const filteredUser = removeFalsy(data.user);
    try {
      const response = await API.put('user', filteredUser);
      dispatch({
        type: 'LOGIN',
        payload: response.data,
      });

      history.push('/myprofile');
    } catch (error) {
      setData({
        ...data,
        error: error.response.data.errors
      });
    }
  }

  return (
    <Form h2Title='update profile' halfwidth>
      <Input name='username' value={data.user.username} onValueChange={handleChange}/>
      {data.error.username && <Error error={data.error.username}/>}
      <Textarea name='bio' value={data.user.bio} onValueChange={handleChange}/>
      {data.error.bio && <Error error={data.error.bio}/>}
      <Input name='email' value={data.user.email} onValueChange={handleChange}/>
      {data.error.email && <Error error={data.error.email}/>}
      <Input name='image' value={data.user.image} onValueChange={handleChange}/>
      {data.error.image && <Error error={data.error.image}/>}
      <Input type='password' name='password' onValueChange={handleChange}/>
      {data.error.password && <Error error={data.error.password}/>}
      <Submit text='update' onClick={handleUpdate}/>
    </Form>
  );
}

export default UpdateProfileForm;