import React from 'react';
import Layout from '../../Layout/Layout';
import { Flex } from '../../Layout/Wrapper';

const banner = {
  title: 'About',
  subText: 'This is what it\'s all about!'
};

function About() {
  return (
    <Layout banner={banner}>
      <Flex box>
        About!
      </Flex>
    </Layout>
  );
}

export default About;
