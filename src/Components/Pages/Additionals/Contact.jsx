import React from 'react';
import Layout from '../../Layout/Layout';
import { Flex } from '../../Layout/Wrapper'

const banner = {
  title: 'Contact',
  subText: "let's get in touch!"
};

function Contact() {
  return (
    <Layout banner={banner}>
      <Flex box>
        Contact me!
      </Flex>
    </Layout>
  );
}

export default Contact;