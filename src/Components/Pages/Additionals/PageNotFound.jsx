import React from 'react';
import { Link } from 'react-router-dom';
import { Flex } from '../../Layout/Wrapper';
import Layout from '../../Layout/Layout';

const banner = {
  title: 'Oh, Oh!',
  subText: 'Looks like something went wrong! :('
};

function PageNotFound() {
  return (
    <Layout banner={banner}>
      <Flex box>
        <h1 style={{fontSize: '4rem',}}>404 - Not Found</h1>
        <Link to="/">Go Home</Link>
      </Flex>
    </Layout>
  );
}

export default PageNotFound;