import React from 'react';
import Layout from '../../../Layout/Layout';
import RegisterForm from './RegisterForm';

const banner = {
  title: 'Register Page',
  subText: 'This is the start of something great!',
};

function Register() {
  return (
    <Layout banner={banner}>
      <RegisterForm />
    </Layout>
  )
}

export default Register;
