import React, {useContext, useEffect, useState} from "react";
import {useHistory} from "react-router-dom";
import {AuthContext} from "../../../../App";
import Form from "../../../Parts/Form/Form";
import API from "../../../../Utils/API";
import Input from "../../../Parts/Form/Input/Input";
import Submit from "../../../Parts/Form/Submit/Submit";
import Error from "../../../Parts/Form/Error/Error";

const initialData = {
  username: '',
  email: '',
  password: '',
  error: '',
};

function RegisterForm() {
  const {dispatch} = useContext(AuthContext);
  const [data, setData] = useState(initialData);
  const history = useHistory();

  useEffect(() => {
    dispatch({
      type: 'LOGOUT'
    });
  }, [dispatch]);

  async function handleRegister() {
    try {
      const response = await API.post('users', {
        username: data.username,
        email: data.email,
        password: data.password,
      });

      dispatch({
        type: 'LOGIN',
        payload: response.data,
      });

      history.push('/');
    }

    catch (error) {
      setData({
        ...data,
        error: error.response.data.errors,
      });
    }
  }

  const handleChange = (e) => {
    const {name, value} = e.currentTarget;
    setData(previousData => ({
      ...previousData,
      [name]: value,
    }))
  };

  return (
    <Form h2Title='Register' narrow>
      <Input name='username' onValueChange={handleChange}/>
      {data.error.username && <Error error={data.error.username}/>}
      <Input name='email' onValueChange={handleChange}/>
      {data.error.email && <Error error={data.error.email}/>}
      <Input type='password' name='password' onValueChange={handleChange}/>
      {data.error.password && <Error error={data.error.password}/>}
      <Submit text='register' onClick={handleRegister}/>
    </Form>
  );
}

export default RegisterForm;