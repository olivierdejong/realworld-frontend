import React, { useContext } from 'react';
import { Redirect } from 'react-router-dom';
import { AuthContext } from '../../../App';

const RequireAuth = ({ children }) => {
  const { isAuthenticated } = useContext(AuthContext).state;
  if (!isAuthenticated) {
    return <Redirect to='/users/login' />;
  }

  return children;
};

export default RequireAuth;