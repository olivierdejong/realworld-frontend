import React, {useContext, useEffect, useState} from "react";
import {useHistory} from "react-router-dom";
import {AuthContext} from "../../../../App";
import API from "../../../../Utils/API";
import Form from "../../../Parts/Form/Form";
import Input from "../../../Parts/Form/Input/Input";
import Submit from "../../../Parts/Form/Submit/Submit";
import Error from '../../../Parts/Form/Error/Error';

const initialData = {
  email: '',
  password: '',
  error: '',
};

function LoginForm() {
  const {dispatch} = useContext(AuthContext);
  const [data, setData] = useState(initialData);
  const history = useHistory();

  useEffect(() => {
    dispatch({
      type: 'LOGOUT'
    });
  }, [dispatch]);

  async function handleLogin() {
    try {
      const response = await API.post('users/login', {
        email: data.email,
        password: data.password,
      });

      dispatch({
        type: 'LOGIN',
        payload: response.data,
      });

      history.push('/');
    } catch (error) {
      setData({
        ...data,
        error: error.response.data.errors,
      });
    }
  }

  const handleChange = (e) => {
    const {name, value} = e.currentTarget;
    setData(previousData => ({
      ...previousData,
      [name]: value,
    }))
  };

  return (
    <Form h2Title='login' narrow>
      <Input name='email' onValueChange={handleChange}/>
      {data.error.email && <Error error={data.error.email}/>}
      <Input type='password' name='password' onValueChange={handleChange}/>
      {data.error.combination && <Error error={data.error.combination}/>}
      <Submit text='login' onClick={handleLogin}/>
    </Form>
  );
}

export default LoginForm;