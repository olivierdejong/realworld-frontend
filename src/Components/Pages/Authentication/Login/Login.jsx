import React from 'react';
import Layout from '../../../Layout/Layout';
import LoginForm from './LoginForm';

const banner = {
  title: 'Login Page',
  subText: 'You do have an account right!?',
};

function Login() {
  return (
    <Layout banner={banner}>
      <LoginForm />
    </Layout>
  )
}

export default Login;
