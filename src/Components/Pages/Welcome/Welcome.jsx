import React from 'react';
import Layout from '../../Layout/Layout';
import WelcomeFeed from './WelcomeFeed';

const banner = {
  title: 'Welcome!',
  subText: 'Start navigating from here'
};

function Welcome() {
  return (
    <Layout banner={banner}>
      <WelcomeFeed />
    </Layout>
  );
}

export default Welcome;