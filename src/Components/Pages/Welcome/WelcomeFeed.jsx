import React, { useState } from 'react';
import { Grid } from '../../Layout/Wrapper';
import SearchBar from '../../Parts/Form/Search/SearchBar';
import FeedWrapper from '../../Parts/Feed/FeedWrapper';
import PopularFeed from '../../Parts/Feed/PopularFeed';

function WelcomeFeed() {
  const [searchData, setSearchData] = useState({
    searchOption: '',
    searchValue: '',
  });

  return (
    <Grid fullwidth align='start' columns='2fr 1fr' columnGap='2rem'>
      <FeedWrapper searchData={searchData} />
      <Grid fullwidth columns='1fr'>
        <SearchBar onSubmit={setSearchData} />
        <PopularFeed numberOfResults={30} items='tags' onSubmit={setSearchData}/>
      </Grid>
    </Grid>
  );
}

export default WelcomeFeed;
