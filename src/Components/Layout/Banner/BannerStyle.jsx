import styled from 'styled-components/macro';
import { fontStyle, fontSize, fontColor } from '../../Styles/Variables/fonts';

export const StyledBanner = styled.div`
  display: flex;
  flex-direction: column;
  background-color: #4267b2;
  color: white;
  text-align: center;
  padding: 30px;
  width: 100%;
`;

export const StyledBannerTitle = styled.div`
  font-family: ${fontStyle.banner};
  color: ${fontColor.title};
  font-size: ${fontSize.M};
  font-weight: 900;
`;

export const StyledBannerText = styled.div`
  font-family: ${fontStyle.banner};
  color: ${fontColor.title};
  font-size: ${fontSize.S}
`;