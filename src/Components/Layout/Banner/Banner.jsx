import React from 'react';
import PropTypes from 'prop-types';
import { StyledBanner, StyledBannerTitle, StyledBannerText } from './BannerStyle'

const Banner = ({ pageTitle, text }) => {
  return (
    <StyledBanner>
      <StyledBannerTitle>{pageTitle}</StyledBannerTitle>
      <StyledBannerText>{text}</StyledBannerText>
    </StyledBanner>
  );
};

Banner.propTypes = {
  pageTitle: PropTypes.string,
  text: PropTypes.string
};

export default Banner;
