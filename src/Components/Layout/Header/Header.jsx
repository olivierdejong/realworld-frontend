import React, { useContext } from 'react';
import { AuthContext } from '../../../App';
import { Flex } from '../Wrapper';
import { StyledHeader, StyledLink } from './HeaderStyle'

function Header() {
  const loggedIn = useContext(AuthContext).state.isAuthenticated;
  const { user } = useContext(AuthContext).state;

  let authLinks = [
      {ref: '/users/login', text: 'Login'},
      {ref: '/users/register', text: 'Register'}
      ];

  if (loggedIn) {
    authLinks = [
      { ref: `/${user.username}/newpost`, text: 'New Article' },
      { ref: '/myprofile', text: user.username },
      { ref: '/users/login', text: 'Logout' },
    ];
  }

  return (
    <StyledHeader>
      <StyledLink to='/'><h1>RealWorld</h1></StyledLink>
      <Flex justify='flex-end'>
        {authLinks.map(
          link => <StyledLink key={link.text} to={link.ref}>{link.text}</StyledLink>
        )}
      </Flex>
    </StyledHeader>
  );
}

export default Header;
