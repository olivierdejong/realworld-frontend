import styled from 'styled-components/macro';

export const Container = styled.div`
  display: flex;
  justify-content: center;
  align-items: start;
  min-height: 30rem;
  margin: 2rem;
`;

export const StyledTagsContainer = styled.div`
  display: flex;
  flex-wrap: wrap-reverse;
  padding: ${props => props.padding}; 
  margin: ${props => props.margin}; 
  justify-content: ${props => props.justify};
  align-items: ${props => props.align};
`;

export const StyledTextContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  padding: ${props => props.padding}; 
  margin: ${props => props.margin}; 
  justify-content: ${props => props.justify};
  align-items: ${props => props.align};
  
  p {
    margin-bottom: 20px;
  }
`;