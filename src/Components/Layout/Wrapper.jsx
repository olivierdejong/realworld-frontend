import styled, { css } from 'styled-components/macro';

export const wrapper = styled.div`
  background-color: ${props => props.bgColor};
  color: ${props => props.color};
  width: ${props => props.width};
  align-items: ${props => props.align};
  justify-content: ${props => props.justify};
  margin: ${props => props.margin};
  margin-top: ${props => props.marginTop};
  margin-bottom: ${props => props.marginBottom};
  padding: ${props => props.padding};
  padding-top: ${props => props.paddingTop};
  padding-bottom: ${props => props.paddingBottom};
  border: ${props => props.border};
  border-top: ${props => props.borderTop};
  border-bottom: ${props => props.borderBottom};
  border-radius: ${props => props.borderRadius};
  
  ${props => props.narrow && css`
   width: 30%;
  `}
  
  ${props => props.halfwidth && css`
   width: 50%;
  `}

  ${props => props.wide && css`
    width: 80%;
  `}

  ${props => props.fullwidth && css`
   width: 100%;
  `}
`;

export const Flex = styled(wrapper)`
  display: flex;
  flex-wrap: ${props => props.wrap};
  
  ${props => props.box && css`
    flex-direction: column;
    align-items: ${props.align || 'center'};
    justify-content: ${props.justify || 'center'};
  `}
  
  ${props => props.row && css`
    flex-direction: row;
    justify-content: ${props.justify || 'space-between'};
    align-items: ${props.align || 'center'};
  `}
`;

export const Grid = styled(wrapper)`
  display: grid;
  grid-template-columns: ${props => props.columns};
  grid-template-rows: ${props => props.rows};
  grid-row-gap: ${props => props.rowGap || '1rem'};
  grid-column-gap: ${props => props.columnGap};
  grid-auto-rows: ${props => props.autoRows};
  grid-auto-columns:${props => props.autoColumns};
`;

export const Block = styled(wrapper)`
  display: block;
`;