import React from 'react';
import { StyledFooter, StyledLink } from './FooterStyle'

function Footer() {
  return (
    <StyledFooter>
      <StyledLink to="/about">About </StyledLink>
      |
      <StyledLink to="/contact"> Contact</StyledLink>
    </StyledFooter>
  );
}

export default Footer;
