import styled from 'styled-components/macro';
import { Link } from 'react-router-dom';

export const StyledFooter = styled.footer`
  background: #333;
  color: #fff;
  text-align: center;
  padding: 0.5rem;
`;

export const StyledLink = styled(Link)`
  color: #fff;
  padding: 0 0.5rem;
`;