import React from 'react';
import PropTypes from "prop-types";
import Header from './Header/Header';
import Footer from './Footer/Footer';
import {Container} from './Container';
import {StyledBanner, StyledBannerTitle, StyledBannerText} from './Banner/BannerStyle'

const Layout = ({banner, children}) => {
  return (
    <div>
      <Header/>
      {banner && <Banner banner={banner}/>}
      <Container>
        {children}
      </Container>
      <Footer/>
    </div>
  );
};

const Banner = ({banner}) => (
  <StyledBanner>
    <StyledBannerTitle>{banner.title}</StyledBannerTitle>
    <StyledBannerText>{banner.subText}</StyledBannerText>
  </StyledBanner>
);

Layout.propTypes = {
  banner: PropTypes.shape({
    pageTitle: PropTypes.string,
    text: PropTypes.string,
  }),
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]),
};

Banner.propTypes = {
  banner: PropTypes.shape({
    title: PropTypes.string,
    subText: PropTypes.string,
  })
};

export default Layout;