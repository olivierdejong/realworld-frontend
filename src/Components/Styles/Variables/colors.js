const color = {
  primary: '#00a591',
  secondary: '#00a591',
  error: '#FF6139',
  shade1: '#B0B0B0',
  shade2: '#20254f'
};

export default color;