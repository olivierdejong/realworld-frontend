export const fontStyle = {
  header: '',
  banner: 'Aclonica',
};

export const fontSize = {
  XS: '0.9rem',
  S: '1.2rem',
  M: '2rem',
  L: '3rem',
  XL: '4rem',
};

export const fontColor = {
  primary: 'black',
  title: 'white',
  error: 'red',
  shade1: '#B0B0B0',
  shade2: '#20254f'
};