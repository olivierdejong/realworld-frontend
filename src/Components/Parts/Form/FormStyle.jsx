import styled, {css} from 'styled-components/macro';

export const FormContainer = styled.div`
  height: ${props => props.height || '100%'};
  width: 50%;
  
  ${props => props.narrow && css`
   width: 30%;
  `}
  
  ${props => props.halfwidth && css`
   width: 50%;
  `}
  
  ${props => props.wide && css`
    width: 80%;
  `}
  
  ${props => props.fullwidth && css`
   width: 100%;
  `}
`;

export const StyledForm = styled.form`
  width: 100%;
  padding: 40px;
  background-color: lightblue;
  border-radius: 10px;
  box-sizing: border-box;
  box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2);
  
  h1, h2, h3, h4, h5 {
    text-transform: capitalize;
    margin: 0 0 20px 0;
    
    ${props => props.center && css`
      text-align: center;
    `}
  }
`;