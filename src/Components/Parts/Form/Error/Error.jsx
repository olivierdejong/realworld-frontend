import React from 'react';
import PropTypes from "prop-types";
import StyledError from './ErrorStyle';

function Error({error}) {
  return (
    <StyledError>
      {error}
    </StyledError>
  );
}

Error.propTypes = {
  error: PropTypes.arrayOf(PropTypes.string),
};

export default Error;