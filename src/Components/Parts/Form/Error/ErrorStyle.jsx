import styled from 'styled-components/macro';

const StyledError = styled.span`
  display: block;
  color: red;
  font-weight: 500;
  width: 100%;
  margin: 20px 0;
`;

export default StyledError;