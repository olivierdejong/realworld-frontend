import React from 'react';
import PropTypes from "prop-types";
import {FormContainer, StyledForm} from './FormStyle';

function Form({center, h1Title, h2Title, h3Title, h4Title, children, onSubmit, name, autocomplete, height, narrow, halfwidth, wide, fullwidth}) {
  return (
    <FormContainer
      height={height}
      narrow={narrow}
      halfwidth={halfwidth}
      wide={wide}
      fullwidth={fullwidth}
    >
      <StyledForm
        center={center}
        name={name}
        onSubmit={onSubmit}
        autocomplete={autocomplete || 'off'}
      >
        {h1Title && <h1>{h1Title}</h1>}
        {h2Title && <h2>{h2Title}</h2>}
        {h3Title && <h3>{h3Title}</h3>}
        {h4Title && <h4>{h4Title}</h4>}
        {children}
      </StyledForm>
    </FormContainer>
  );
}

Form.propTypes = {
  center: PropTypes.bool,
  h1Title: PropTypes.string,
  h2Title: PropTypes.string,
  h3Title: PropTypes.string,
  h4Title: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]),
  onSubmit: PropTypes.func,
  name: PropTypes.string,
  autocomplete: PropTypes.bool,
  height: PropTypes.string,
  narrow: PropTypes.bool,
  halfwidth: PropTypes.bool,
  wide: PropTypes.bool,
  fullwidth: PropTypes.bool,
};

export default Form;