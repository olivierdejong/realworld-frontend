import styled, { css } from 'styled-components/macro';

export const SubmitContainer = styled.div`
  width: 100%;

  ${props => props.center && css`
    display: flex;
    justify-content: center;
  `}
  
  ${props => props.end && css`
    display: flex;
    justify-content: end;
  `}
`;

export const StyledSubmit = styled.button`
  display: block;
  background-color: #4267B2;
  color: #fff;
  font-size: .9rem;
  border: 0;
  border-radius: 5px;
  height: 40px;
  padding: 0 20px;
  cursor: pointer;
  box-sizing: border-box;
  text-transform: capitalize;
`;

export default StyledSubmit;