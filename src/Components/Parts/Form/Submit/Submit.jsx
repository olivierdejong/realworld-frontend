import React from 'react';
import PropTypes from "prop-types";
import {SubmitContainer, StyledSubmit } from './SubmitStyle';

function Submit({center, end, text, onClick}) {
  const handleClick = (e) => {
    e.preventDefault();

    if (onClick) {
      onClick(e);
    }
  };

  return (
    <SubmitContainer
      center={center}
      end={end}>
      <StyledSubmit
        type='submit'
        onClick={handleClick} >
        {text || 'submit'}
      </StyledSubmit>
    </SubmitContainer>
  );
}

Submit.propTypes = ({
  center: PropTypes.bool,
  end: PropTypes.bool,
  text: PropTypes.string,
  onClick: PropTypes.func,
});

export default Submit