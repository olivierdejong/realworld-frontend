import React, {useState} from 'react';
import PropTypes from "prop-types";
import StyledTextarea from './TextareaStyle';
import {StyledLabel} from "../Input/InputStyle";

function Textarea({label, name, autoFocus, cols, rows, maxlength, disabled, readOnly, placeholder, required, wrap, value, onValueChange}) {
  const [state, setState] = useState(value || '');

  const onChange = (e) => {
    setState(e.currentTarget.value);

    if (onValueChange) {
      onValueChange(e);
    }
  };

  return (
    <StyledLabel htmlFor={name}>
      {label || name}
      <StyledTextarea
        id={name}
        name={name}
        value={state}
        autoFocus={autoFocus}
        cols={cols}
        rows={rows}
        maxlength={maxlength}
        wrap={wrap}
        disabled={disabled}
        readOnly={readOnly}
        placeholder={placeholder || name}
        required={required}
        onChange={onChange} />
    </StyledLabel>

  );
}

Textarea.propTypes = {
  cols: PropTypes.number,
  rows: PropTypes.number,
  maxlength: PropTypes.number,
  wrap: PropTypes.string,
  value: PropTypes.string,
  label: PropTypes.string,
  name: PropTypes.string,
  placeholder: PropTypes.string,
  disabled: PropTypes.bool,
  readOnly: PropTypes.bool,
  autoFocus: PropTypes.bool,
  required: PropTypes.bool,
  onValueChange: PropTypes.func,
};

export default Textarea;