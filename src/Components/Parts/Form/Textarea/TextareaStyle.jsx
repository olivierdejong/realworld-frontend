import styled from 'styled-components/macro';

const StyledTextarea = styled.textarea`
  white-space: pre-line;
  background-color: #eee;
  height: 40px;
  border-radius: 5px;
  border: 1px solid #ddd;
  margin: 10px 0 20px 0;
  padding: 20px;
  box-sizing: border-box;
  width: 100%;
  min-height: 100px;
  resize: none;
`;

export default StyledTextarea;