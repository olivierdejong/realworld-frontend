import styled from 'styled-components/macro';

export const SearchBarContainer = styled.div`
  padding: 2rem 3rem;
  background-color: lightblue;
  border-radius: 10px;
  box-sizing: border-box;
  box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2);
`;

export const StyledSearchBar = styled.form`
  width: 100%;
  
  h1, h2, h3, h4, h5 {
    display: flex;
    justify-content: center;
    margin-bottom: 0.5rem;
  }

  input[type=text] {
    display: block;
    width: 100%;
    background-color: #eee;
    height: 40px;
    border-radius: 5px;
    border: 1px solid #ddd;
    margin: 10px 0 20px 0;
    padding: 20px;
    box-sizing: border-box;
  }
`;

export const StyledFieldSet = styled.fieldset`
  border: 1px solid #ddd;
  border-radius: 5px;
  padding: 10px;
  margin: 10px 0 1rem;
  
  legend {
    padding: 0 10px;
  }
  
  label {
    padding-right: 20px;
  }
  
  input {
    margin-right: 10px;
  }
`;