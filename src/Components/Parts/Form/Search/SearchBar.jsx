import React, {useState} from 'react';
import PropTypes from "prop-types";
import Submit from '../Submit/Submit';
import Error from '../Error/Error';
import { SearchBarContainer, StyledSearchBar, StyledFieldSet } from "./SearchBarStyle";

const initialData = {
  searchOption: 'author',
  searchValue: '',
  error: '',
};

function SearchBar({onSubmit}) {
  const [data, setData] = useState(initialData);

  function handleChange(e) {
    setData({
      ...data,
      [e.target.name]: e.target.value,
    });
  }

  function handleSubmit(e) {
    e.preventDefault();

    if (data.searchOption === '') {
      setData({
        ...data,
        error: 'Choose Search Domain'
      });
      return
    }

    if (data.searchValue === '') {
      setData({
        ...data,
        error: 'Enter Search Value'
      });
      return
    }

    onSubmit({
      ...data
    });

    setData({
      ...data,
      searchValue: '',
      error: ''
    });
  }

  return (
    <SearchBarContainer>
      <StyledSearchBar onSubmit={handleSubmit}>
        <h1>Search Articles</h1>
        <input type='text' name='searchValue' value={data.searchValue} onChange={handleChange}/>
        <StyledFieldSet>
          <legend>Search Domain</legend>
          <label htmlFor='searchOption'>
            <input type='radio' value='author' name='searchOption' checked={data.searchOption === 'author'} onChange={handleChange}/>
            Author
          </label>
          <label htmlFor='searchOption'>
            <input type='radio' value='tag' name='searchOption' checked={data.searchOption === 'tag'} onChange={handleChange}/>
            Tag
          </label>
        </StyledFieldSet>
        {data.error && <Error error={data.error} />}
        <Submit center text='Search' onClick={handleSubmit}/>
      </StyledSearchBar>
    </SearchBarContainer>
  );
}

SearchBar.propTypes = {
  onSubmit: PropTypes.func,
};

export default SearchBar;
