import React, {useState} from 'react';
import PropTypes from "prop-types";
import {StyledInput, StyledLabel} from './InputStyle';

function Input({splitComma, id, label, name, type, disabled, readOnly, autoFocus, placeholder, required, value, onValueChange}) {
  const [state, setState] = useState(value || '');
  const onChange = (e) => {
    if (splitComma) {

      // replace ' ' with ', ' except when comma is already typed.
      const val = e.target.value;
      if (val.slice(val.length - 1) === ' ' && val.slice(val.length - 2) !== ', ') {
        e.currentTarget.value = `${val.slice(0, val.length - 1)}, `;
      }
    }

    setState(e.currentTarget.value);
    if (onValueChange) {
      onValueChange(e);
    }
  };

  return (
    <StyledLabel htmlFor={name}>
      {label || name}
      <StyledInput
        id={id}
        name={name}
        value={state}
        type={type || 'text'}
        disabled={disabled}
        readOnly={readOnly}
        autoFocus={autoFocus}
        placeholder={placeholder || name}
        required={required}
        onChange={onChange} />
    </StyledLabel>
  );
}

Input.propTypes = {
  id: PropTypes.number,
  value: PropTypes.string,
  label: PropTypes.string,
  name: PropTypes.string,
  type: PropTypes.string,
  placeholder: PropTypes.string,
  splitComma: PropTypes.bool,
  disabled: PropTypes.bool,
  readOnly: PropTypes.bool,
  autoFocus: PropTypes.bool,
  required: PropTypes.bool,
  onValueChange: PropTypes.func,
};

export default Input;