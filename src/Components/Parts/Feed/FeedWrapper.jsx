import React, {useContext, useState, useEffect} from 'react';
import PropTypes from 'prop-types';
import {Block} from '../../Layout/Wrapper';
import Paginator from './Paginator';
import Feed from './Feed';
import {AuthContext} from '../../../App';
import API from '../../../Utils/API';

function FeedWrapper({ searchData, profile }) {
  const { isAuthenticated } = useContext(AuthContext).state;
  const { username } = profile || '';
  const { searchOption, searchValue } = searchData || {searchOption: '', searchValue: ''};
  const tagsLimit = 4;

  const globalTabs = [
    (isAuthenticated && {title: 'Your Feed', uri: '/feed'}),
    {title: 'Global Feed'},
  ];

  const personalTabs = [
    {title: 'My Articles', query: `author=${username}`},
    {title: 'Favorited Articles', query: `favorited=${username}`},
  ];

  const defaultTabs = username ? personalTabs : globalTabs;

  const [tabs, setTabs] = useState({
    tabs: [...defaultTabs].filter(Boolean),
    activeTab: 0,
  });

  const [articles, setArticles] = useState({
    articles: [],
    errorMessage: '',
  });

  const [values, setValues] = useState({
    limit: 4,
    offset: 0,
  });

  const [pagination, setPagination] = useState({});

  function getNewTabOrFalse(existingTabs, tab) {
    let i;
    for (i = 0; i < existingTabs.length; i += 1) {
      if (tab.title === existingTabs[i].title) {
        return {index: i, tab: false}
      }
    }

    return {index: existingTabs.length, tab};
  }

  useEffect(() => {
    if (username) {
      setTabs({
        tabs: [
          {title: 'My Articles', query: `author=${username}`},
          {title: 'Favorited Articles', query: `favorited=${username}`},
        ],
        activeTab: 0,
      })
    }

    if (searchOption) {
      const tab = {title: `#${searchOption} '${searchValue}'`, query: `${searchOption}=${searchValue}`};

      setTabs(previousTabs => {
        const newTab = getNewTabOrFalse(previousTabs.tabs, tab);

        // Limit number of tags
        if (newTab.tab && previousTabs.tabs.length > tagsLimit){
          const indexToDelete = (isAuthenticated || username) ? 2 : 1;
          previousTabs.tabs.splice(indexToDelete, 1);
          newTab.index -= 1;
        }

        return {
          tabs: [
            ...previousTabs.tabs,
            newTab.tab,
          ].filter(Boolean),
          activeTab: newTab.index,
        }
      });
    }
  }, [username, isAuthenticated, searchOption, searchValue]);

  useEffect(() => {
    async function fetchData() {
      const urlValues = `limit=${values.limit}&offset=${values.offset}`;
      const tab = tabs.tabs[tabs.activeTab];

      let urlUri = '';
      if (typeof (tab.uri) !== 'undefined') {
        urlUri = tab.uri
      }

      let urlSearch = '';
      if (typeof (tab.query) !== 'undefined') {
        urlSearch = tab.query
      }

      try {
        const response = await API.get(
          `articles${urlUri}?${urlSearch}&${urlValues}`
        );

        setArticles({
          articles: response.data.articles,
          errorMessage: '',
        });

        setPagination(response.data.meta);
      }

      catch (error) {
        setArticles({
          articles: [],
          errorMessage: error.data,
        });
      }
    }

    fetchData()
  }, [values, tabs]);

  function changePagination(newPage) {
    setValues({
      ...values,
      offset: values.limit * (newPage -1),
    })
  }

  function changeActiveTab(i) {
    setTabs(previousTabs => {
      return {
        ...previousTabs,
        activeTab: i
      }
    });
  }

  return (
    <Block>
      <Paginator tabs={tabs} pagination={pagination} onTabClick={changeActiveTab} onPageClick={changePagination}/>
      {articles && <Feed articles={articles}/>}
    </Block>
  );
}

FeedWrapper.propTypes = {
  searchData: PropTypes.shape({
    searchOption: PropTypes.string,
    searchValue: PropTypes.string,
  }),
  profile: PropTypes.shape({
    username: PropTypes.string,
    bio: PropTypes.string,
    image: PropTypes.string,
  }),
};

export default FeedWrapper;
