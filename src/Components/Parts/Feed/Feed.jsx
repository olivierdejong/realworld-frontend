import React from 'react';
import PropTypes from 'prop-types';
import {Block} from '../../Layout/Wrapper'
import ArticleCard from '../../Pages/Article/Parts/ArticleCard';

function Feed({articles}) {
  return (
    <Block>
      {articles.errorMessage &&
      <span>{articles.errorMessage}</span>}
      {articles &&
      articles.articles.map(article => <ArticleCard key={article.id} article={article}/>)}
    </Block>
  );
}

Feed.propTypes = {
  articles: PropTypes.shape({
    articles: PropTypes.arrayOf(
      PropTypes.shape({
        slug: PropTypes.string,
        title: PropTypes.string,
        createdAt: PropTypes.string,
        description: PropTypes.string,
        tagList: PropTypes.arrayOf(PropTypes.shape({
          id: PropTypes.number,
          name: PropTypes.string,
        })),
        favorited: PropTypes.bool,
        favoritesCount: PropTypes.number,
        author: PropTypes.shape({
          username: PropTypes.string,
          bio: PropTypes.string,
          image: PropTypes.string,
          following: PropTypes.bool
        }),
      })
    ),
    errorMessage: PropTypes.string,
  })
};

export default Feed;
