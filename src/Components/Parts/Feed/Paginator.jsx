import React, {useState, useEffect} from 'react';
import PropTypes from "prop-types";
import {Tab, Tabs, TabPanel, TabList} from 'react-tabs';
import {Flex} from '../../Layout/Wrapper'

const newStyle = {
  margin: '0 0 1rem',
  borderBottom: '1px solid lightgrey',
  padding: '0 12px',
};

function Paginator({tabs, pagination, onTabClick, onPageClick}) {
  const {tabs: tabList, activeTab} = tabs;
  const [paginator, setPaginator] = useState(pagination);
  const {current_page: currentPage, last_page: lastPage} = paginator;
  const firstPage = 1;
  const previousPage = (currentPage - 1 > 0) ? currentPage - 1 : null;
  const nextPage = (currentPage + 1 < lastPage + 1) ? currentPage + 1 : null;

  useEffect(() => {
    setPaginator({
      ...pagination,
      current_page: pagination.current_page,
    });
  }, [pagination]);

  function handleClick(e) {
    onPageClick(e.target.value);
  }

  return (
    <Flex row style={newStyle}>
      <Tabs selectedIndex={activeTab} onSelect={index => onTabClick(index)}>
        <TabList>
          {tabList.map((tab) => {
            return (<Tab key={tab.title}> {tab.title} {tab.button} </Tab>)
          })}
        </TabList>
        {tabList.map((tab) => {
          return (<TabPanel key={tab.title}/>)
        })}
      </Tabs>
      <Flex wrap='noWrap'>
        <div className="pagination">
          <button
            type='button' value={previousPage || firstPage} onClick={handleClick}>&laquo;
          </button>
          {(previousPage > firstPage) &&
          <button
            type='button' value={firstPage} onClick={handleClick}>{firstPage}
          </button>}
          {(previousPage > firstPage + 1) &&
          <button
            type='button'>...
          </button>}
          {previousPage &&
          <button
            type='button' value={previousPage} onClick={handleClick}>{previousPage}
          </button>}
          <button
            type='button' style={{borderColor: 'green', backgroundColor: 'lightgreen'}} value={currentPage}
            onClick={handleClick}>{currentPage}
          </button>
          {nextPage &&
          <button
            type='button' value={nextPage} onClick={handleClick}>{nextPage}
          </button>}
          {(nextPage && nextPage < lastPage - 1) &&
          <button
            type='button'>...
          </button>}
          {(nextPage && nextPage < lastPage) &&
          <button
            type='button' value={lastPage} onClick={handleClick}>{lastPage}
          </button>}
          <button
            type='button' value={nextPage || lastPage} onClick={handleClick}>&raquo;
          </button>
        </div>
      </Flex>
    </Flex>
  );
}

Paginator.propTypes = {
  tabs: PropTypes.shape({
    tabs: PropTypes.arrayOf(
      PropTypes.shape({
        title: PropTypes.string,
        query: PropTypes.string,
        uri: PropTypes.string,
      })
    ),
    activeTab: PropTypes.number,
  }),
  pagination: PropTypes.shape({
    current_page: PropTypes.number,
  }),
  onTabClick: PropTypes.func,
  onPageClick: PropTypes.func,
};

export default Paginator;
