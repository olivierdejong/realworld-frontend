import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import API from '../../../Utils/API';
import Form from '../Form/Form'
import { StyledTag } from '../Button/ButtonStyle'

const initialData = {
  itemList: [],
  error: '',
};

function PopularFeed({ numberOfResults, items, onSubmit }) {
  const [data, setData] = useState(initialData);
  const itemName = items.charAt(0).toLowerCase() + items.substring(1);

  useEffect(() => {
    async function fetchData() {
      try {
        const response = await API.get(`/${itemName}/count?limit=${numberOfResults}`);
        setData({
          itemList: response.data,
          error: '',
        });
      }

      catch (error) {
        setData({
          itemList: [],
          error: error.data,
        })
      }
    }

    fetchData();
    }, [itemName, numberOfResults]
  );

  function onClick(name) {
    onSubmit({
      searchOption: 'tag',
      searchValue: name,
    })
  }

  return (
    <Form fullwidth h1Title={`popular ${items}`} center>
      <>
        {data.error &&
        <span>{data.error}</span>}
        {data.itemList
        && data.itemList.map(element => <StyledTag type='button' key={element.id} onClick={() => onClick(element.name)}>{element.name}</StyledTag>)}
      </>
    </Form>
  );
}

PopularFeed.propTypes = {
  numberOfResults: PropTypes.number,
  items: PropTypes.string,
  onSubmit: PropTypes.func,
};

export default PopularFeed;
