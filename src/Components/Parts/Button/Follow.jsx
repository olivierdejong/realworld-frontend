import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import API from '../../../Utils/API';
import { StyledButton } from './ButtonStyle'

const initialMessage = {
  string: '',
  errorMessage: '',
};

function Follow({ profile }) {
  const { username, following } = profile;
  const [isFollowing, setFollowing] = useState(following);
  const [message, setMessage] = useState(initialMessage);

  useEffect(() => {
    setMessage({
      string: `${isFollowing ? 'Unf' : 'F'  }ollow ${username}`,
      errorMessage: '',
    });
  }, [username, isFollowing]);

  async function handleClick(e) {
    e.preventDefault();

    try{
      const profileUpdated = isFollowing ?
        await API.delete(`profiles/${username}/follow`) :
        await API.post(`profiles/${username}/follow`);
      const { following: fol } = profileUpdated.data.profile;
      setFollowing(fol);
    }

    catch (error) {
      setMessage({
        string: '',
        errorMessage: error.response.data.error,
      })
    }
  }

  return (
    <StyledButton
      type='button'
      color='green'
      onClick={handleClick}
    >
      {message.string || message.errorMessage}
    </StyledButton>
  );
}

Follow.propTypes = {
  profile: PropTypes.shape({
    username: PropTypes.string,
    following: PropTypes.bool
  }),
};

export default Follow;
