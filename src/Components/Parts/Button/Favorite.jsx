import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import API from '../../../Utils/API';
import { StyledButton } from './ButtonStyle'

const initialMessage = {
  string: '',
  errorMessage: '',
};

function Favorite({ article }) {
  const { slug, favorited, favoritesCount } = article;
  const [hasFavorited, setFavorite] = useState(favorited);
  const [count, setFavoritesCount] = useState(favoritesCount);
  const [message, setMessage] = useState(initialMessage);

  useEffect(() => {
    setMessage({
      string: `${hasFavorited? 'Unf' : 'F'  }avorite Article (${count})`,
      errorMessage: ''
    });
  }, [count, hasFavorited]);

  async function handleClick(e) {
    e.preventDefault();

    try{
      const updatedArticle = hasFavorited ?
        await API.delete(`articles/${slug}/favorite`) :
        await API.post(`articles/${slug}/favorite`);
      const { favorited: fav, favoritesCount: favCount } = await updatedArticle.data.article;
      setFavorite(fav);
      setFavoritesCount(favCount);
    }

    catch (error) {
      setMessage({
        string: '',
        errorMessage: error.response.data.error,
      })
    }
  }

  return (
    <StyledButton
      type='button'
      color='green'
      onClick={handleClick}
    >
      {message.string || message.errorMessage}
    </StyledButton>
  );
}

Favorite.propTypes = {
  article: PropTypes.shape({
    slug: PropTypes.string,
    favorited: PropTypes.bool,
    favoritesCount: PropTypes.number,
  })
};

export default Favorite;
