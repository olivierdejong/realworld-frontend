import React from 'react';
import {useHistory, useLocation} from 'react-router-dom';
import PropTypes from 'prop-types';
import {StyledButton} from './ButtonStyle'

function Edit({text}) {
  const history = useHistory();
  const {pathname} = useLocation();

  return (
    <StyledButton
      type='button'
      color='green'
      onClick={() => history.push(`${pathname}/edit`)}
    >
      {text}
    </StyledButton>
  );
}

Edit.propTypes = {
  text: PropTypes.string,
};

export default Edit;
