import styled, { css } from 'styled-components/macro';

export const StyledButton = styled.button`
  border-radius: 0.5rem;
  padding: 0.5rem 0.4rem;
  min-width: 6rem;
  color: ${props => props.color};
  border-color: ${props => props.color};
`;

export const StyledTag = styled.button`
  min-width: 2.5rem;
  margin: 0.1rem;
  padding: 0.3rem 0.6rem;
  border-radius: 1rem;
  color: white;
  background-color: #4267B2;
  
  ${props => props.discreet && css`
    color: lightgrey;
    border-color: lightgrey;
    background-color: white;
  `}
`;