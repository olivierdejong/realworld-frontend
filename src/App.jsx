import React, { createContext, useReducer } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import './App.css';
import 'typeface-roboto';
import Welcome from './Components/Pages/Welcome/Welcome';
import Login from './Components/Pages/Authentication/Login/Login';
import Register from './Components/Pages/Authentication/Register/Register';
import Article from './Components/Pages/Article/Article';
import Profile from './Components/Pages/Profile/Profile';
import MyProfile from './Components/Pages/Profile/MyProfile';
import About from './Components/Pages/Additionals/About';
import Contact from './Components/Pages/Additionals/Contact';
import PageNotFound from './Components/Pages/Additionals/PageNotFound';
import { reducer, initialState } from './Utils/AuthReducer';
import RequireAuth from "./Components/Pages/Authentication/RequireAuth";
import UpdateArticle from "./Components/Pages/Article/UpdateArticle";
import CreateArticle from "./Components/Pages/Article/CreateArticle";
import UpdateProfile from "./Components/Pages/Profile/UpdateProfile";

function App() {
  const [state, dispatch] = useReducer(reducer, initialState);
  return (
    <AuthContext.Provider value={{ state, dispatch }}>
      <div className="App">
        <Router>
          <Switch>
            <Route exact path="/" component={Welcome} />
            <Route path="/users/login" component={Login} />
            <Route path="/users/register" component={Register} />
            <Route path="/profiles/:username" component={Profile} />
            <Route exact path="/articles/:slug" component={Article} />
            <Route path="/about" component={About} />
            <Route path="/contact" component={Contact} />
            <RequireAuth>
              <Route exact path="/myprofile" component={MyProfile} />
              <Route exact path="/myprofile/edit" component={UpdateProfile} />
              <Route exact path="/:username/newpost" component={CreateArticle} />
              <Route exact path="/articles/:slug/edit" component={UpdateArticle} />
            </RequireAuth>
            <Route path="*" component={PageNotFound} />
          </Switch>
        </Router>
      </div>
    </AuthContext.Provider>
  );
}
export default App;

export const AuthContext = createContext('');
